data "aws_ami" "latest_ubuntu" {
  owners = ["099720109477"]
  most_recent = true
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }  
}

# Generating a private_key
resource "tls_private_key" "endptkey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "private-key" {
  content  = tls_private_key.endptkey.private_key_pem
  filename = "endptkey.pem" #naming our key pair so that we can connect via ssh into our instances
}

resource "aws_key_pair" "deployer" {
  key_name   = "endptkey"
  public_key = tls_private_key.endptkey.public_key_openssh
}

resource "aws_instance" "public_instance" {
  ami                    = data.aws_ami.latest_ubuntu.id
  instance_type          = "t3.micro"
  subnet_id              = aws_subnet.My_VPC_Subnet_Public.id
  iam_instance_profile = aws_iam_instance_profile.ec2profile.name
  key_name               = var.key_name # insert your key file name here
  vpc_security_group_ids = [aws_security_group.My_VPC_Security_Group_Public.id]
  tags = {
    Name = "public_instance"
  }
}

resource "aws_instance" "private_instance" {
  ami                    = data.aws_ami.latest_ubuntu.id
  instance_type          = "t3.micro"
  subnet_id              = aws_subnet.My_VPC_Subnet_Private.id
  key_name               = var.key_name # insert your key file name here
  vpc_security_group_ids = [aws_security_group.My_VPC_Security_Group_Private.id]
  iam_instance_profile   = aws_iam_instance_profile.ec2profile.name
  user_data              = templatefile("user-data.sh.tpl", {
    flask_bucket = var.bucket_name
  })
  user_data_replace_on_change = true

  tags = {
    Name = "private_instance"
  }
}

