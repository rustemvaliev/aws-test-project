#!/bin/bash
apt-get  -y update
apt-get -y install python3-pip git
git clone https://github.com/dianephan/flask-aws-storage.git app && cd $_
#git checkout 6a374264a707bb4fbfce25ddc9e4ad81cec1ada4
pip install -r requirements.txt
sed -i 's/lats-image-data/${flask_bucket}/g' app.py
flask run --host=0.0.0.0 --port=80