data "aws_availability_zones" "available" {}

resource "aws_elb" "elb" {
  name    = "terraform-elb"
  subnets = [aws_subnet.My_VPC_Subnet_Private.id, aws_subnet.My_VPC_Subnet_Public.id]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 120
  }

  instances                   = [aws_instance.private_instance.id]
  cross_zone_load_balancing   = true
  idle_timeout                = 3600
  connection_draining         = true
  connection_draining_timeout = 3600

}

output "elb_id" {
  value = aws_elb.elb.dns_name
}